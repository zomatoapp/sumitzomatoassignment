//
//  Network.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 29/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import Foundation


class Network
{
    func performURLRequestAndGetMeJsonData(urlString: String , _ completionBlock: @escaping (Any?) -> Void)
    {
        guard let url = URL(string: urlString)
            else
        {
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async
            {
                let session = URLSession.shared
                session.dataTask(with: url) { (data , response , error) in
                    if let response = response
                    {
                        print(response)
                    }
                    if let data = data
                    {
                        do
                        {
                            let jsonData = try JSONSerialization.jsonObject(with: data, options: [])
                            completionBlock(jsonData)
                        }
                        catch
                        {
                            completionBlock(nil)
                            print(error)
                        }
                    }
                    }.resume()
        }
    }
    
    func downloadImage(fromURL urlString: String , AndPerformBlock completionBlock: @escaping (Data?) -> ())
    {
        guard let url = URL(string: urlString)
            else
        {
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async
            {
                let session = URLSession.shared
                session.dataTask(with: url) { (data , response , error) in
                                                if let response = response
                                                {
                                                    completionBlock(data)
                                                    print(response)
                                                }
                                            }.resume()
        }
    }
}
