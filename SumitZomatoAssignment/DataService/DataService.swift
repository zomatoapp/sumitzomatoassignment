//
//  DataService.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 29/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import Foundation

class DataService
{
    private static let _instance = DataService()
    
    static var instance: DataService
    {
        return _instance
    }
    
    var allImagesModelSectionArray = [[ImagesModel]]()
    
    func parseJSONData(jsonData: Any , AndContentWidth contentWidth: Int)
    {
        let keyValueDictionary  = jsonData as! [String: Any]
        print(keyValueDictionary)
        let dataDictionaryArray = keyValueDictionary["data"] as! [Any]
        print(dataDictionaryArray)
        var currentIndex        = 0
        var currentType         = 1
        var colomnsDataRowModelArray = [ImagesModel]()
        for dataDict in dataDictionaryArray
        {
            let imagesKeyDataDict        = dataDict as! [String: Any]
            print(imagesKeyDataDict)
            let imagesDataDict           = imagesKeyDataDict["images"] as! [String: Any]
            print(imagesDataDict)
            let originalStillDict        = imagesDataDict["original_still"] as! [String: String]
            print(originalStillDict)
            
            let imageModel               = ImagesModel()
            imageModel.imageUrlString    = originalStillDict["url"]
            imageModel.imageWidth        = Int(originalStillDict["width"]!)
            imageModel.imageHeight       = Int(originalStillDict["height"]!)
            
            let originalGifDict          = imagesDataDict["original"] as! [String: String]
            imageModel.imageGIFUrlString = originalGifDict["url"]
            
            if currentIndex == 0
            {
                currentType  = self.getCurrentCellType(width: imageModel.imageWidth!, InContentWidth: contentWidth)
                currentIndex = currentType
            }
            imageModel.cellType  = currentType
            currentIndex        -= 1
            
            colomnsDataRowModelArray.append(imageModel)
            
            if currentIndex == 0
            {
                self.allImagesModelSectionArray.append(colomnsDataRowModelArray)
                colomnsDataRowModelArray.removeAll()
            }
        }
    }
    
    func getCurrentCellType(width: Int , InContentWidth contentWidth: Int) -> Int
    {
        if width < contentWidth/2
        {
            return 3
        }
        else if width > contentWidth/2 && width < contentWidth
        {
            return 2
        }
        return 1
    }
}
