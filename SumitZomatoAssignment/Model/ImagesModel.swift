//
//  ImageModel.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 29/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class ImagesModel
{
    var imageWidth: Int?
    var imageHeight: Int?
    var imageUrlString: String?
    var imageGIFUrlString: String?
    var stillImage: UIImage?
    var gifImage: UIImage?
    var cellType: Int?
}
