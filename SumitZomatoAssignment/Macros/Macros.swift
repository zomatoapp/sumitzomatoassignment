//
//  Macros.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 29/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class Macros
{
    static let GifiURL                            = "http://api.giphy.com/v1/gifs/search?q=ryan+gosling&api_key=ysoz82P6R8wGfbkdEi8JRInu64VmzNpP&limit=50"
    static let CollectionViewLineSpacing: CGFloat = 10.0
    
    static let Type1CellIdentifier                = "type1CellIdentifier"
    static let Type2CellIdentifier                = "type2CellIdentifier"
    static let Type3CellIdentifier                = "type3CellIdentifier"
    
    static let CellHeight: CGFloat                = 150
    
    static let CellPadding:CGFloat                = 5
    
    static let CornerRadius: CGFloat              = 5
    
    static let GIFCornerRadius: CGFloat           = 10
    
    static let InsetFromSuperView:CGFloat         = 5
}
