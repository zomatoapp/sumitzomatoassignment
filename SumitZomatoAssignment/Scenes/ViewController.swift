//
//  ViewController.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 29/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class ViewController: UIViewController
{
    //MARK - IBOutlets
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    
    //MARK: - Variables
    let transition = CustomAppStoreAnimation()
    var currentImageRelativeToMainViewFrame: CGRect?

    //MARK: - Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.setupUI()

        let contentWidth = Int(UIScreen.main.bounds.size.width)
        //Fetching Json
        let network = Network()
        network.performURLRequestAndGetMeJsonData(urlString: Macros.GifiURL)
        { [weak self] json in
            if let jsonData = json
            {
                DataService.instance.parseJSONData(jsonData: jsonData , AndContentWidth: contentWidth)
                DispatchQueue.main.async
                {
                    self?.imagesCollectionView.reloadData()
                }
            }
        }
        
        transition.dismissCompletion = {
        }
    }

    //MARK: - UI Methods
    private func setupUI()
    {
        self.view.addSubview(self.imagesCollectionView)
        
        self.imagesCollectionView.register(Type1CollectionViewCell.self , forCellWithReuseIdentifier: Macros.Type1CellIdentifier)
        self.imagesCollectionView.register(Type2CollectionViewCell.self , forCellWithReuseIdentifier: Macros.Type2CellIdentifier)
        self.imagesCollectionView.register(Type3CollectionViewCell.self , forCellWithReuseIdentifier: Macros.Type3CellIdentifier)
        self.setupImagesCollectionView()

        self.imagesCollectionView.delegate   = self
        self.imagesCollectionView.dataSource = self
    }
    
    private func setupImagesCollectionView()
    {
        let customLayout                               = UICollectionViewFlowLayout()
        customLayout.minimumLineSpacing                = Macros.CollectionViewLineSpacing
        customLayout.scrollDirection                   = .vertical
        self.imagesCollectionView.frame                = .zero
        self.imagesCollectionView.collectionViewLayout = customLayout
        self.imagesCollectionView.backgroundColor      = UIColor.clear
    }
}

extension ViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout
{
    func numberOfSections(in collectionView: UICollectionView) -> Int
    {
        return DataService.instance.allImagesModelSectionArray.count
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return DataService.instance.allImagesModelSectionArray[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].cellType == 1
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Macros.Type1CellIdentifier , for: indexPath) as! Type1CollectionViewCell
            if DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage == nil
            {
                let network = Network()
                network.downloadImage(fromURL: DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].imageUrlString!)
                { (data) in
                    if let unwrappedData = data
                    {
                        let downloadedImage = UIImage(data: unwrappedData)
                        DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage = downloadedImage
                        DispatchQueue.main.async
                        {
                            cell.loadWithImage(named: downloadedImage!)
                        }
                    }
                }
            }
            else
            {
                cell.loadWithImage(named: DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage!)
            }
            return cell
        }
        else if DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].cellType == 2
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Macros.Type2CellIdentifier , for: indexPath) as! Type2CollectionViewCell
            if DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage == nil
            {
                let network = Network()
                network.downloadImage(fromURL: DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].imageUrlString!)
                { (data) in
                    if let unwrappedData = data
                    {
                        let downloadedImage = UIImage(data: unwrappedData)
                        DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage = downloadedImage
                        DispatchQueue.main.async
                        {
                            cell.loadWithImage(named: downloadedImage!)
                        }
                    }
                }
            }
            else
            {
                cell.loadWithImage(named: DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage!)
            }
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Macros.Type3CellIdentifier , for: indexPath) as! Type3CollectionViewCell
        if DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage == nil
        {
            let network = Network()
            network.downloadImage(fromURL: DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].imageUrlString!)
            { (data) in
                if let unwrappedData = data
                {
                    let downloadedImage = UIImage(data: unwrappedData)
                    DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage = downloadedImage
                    DispatchQueue.main.async
                    {
                        cell.loadWithImage(named: downloadedImage!)
                    }
                }
            }
        }
        else
        {
            cell.loadWithImage(named: DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].stillImage!)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        print(indexPath.section)
        print(indexPath.row)
        let cell                            = collectionView.cellForItem(at: indexPath)
        currentImageRelativeToMainViewFrame = collectionView.convert((cell?.frame)! , to: self.view)
        print(self.currentImageRelativeToMainViewFrame!)
        
        let gifImageVC                      = GIFImageViewController()
        gifImageVC.selectedImageSection     = indexPath.section
        gifImageVC.selectedImageRow         = indexPath.row
        gifImageVC.transitioningDelegate    = self
        self.present(gifImageVC , animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellType = CGFloat(DataService.instance.allImagesModelSectionArray[indexPath.section][indexPath.row].cellType!)
        let width    = self.imagesCollectionView.frame.size.width / cellType - ((cellType) * Macros.CellPadding)
        return CGSize(width: width , height: Macros.CellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsets(top: Macros.CellPadding , left: Macros.CellPadding , bottom: Macros.CellPadding , right: Macros.CellPadding)
    }
}

extension ViewController: UIViewControllerTransitioningDelegate
{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        transition.originFrame  = self.currentImageRelativeToMainViewFrame!
        
        transition.presenting   = true
        
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning?
    {
        transition.presenting = false
        return transition
    }
}
