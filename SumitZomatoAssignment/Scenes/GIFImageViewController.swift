//
//  GIFImageViewController.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 30/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class GIFImageViewController: UIViewController , UIViewControllerTransitioningDelegate
{
    //MARK: - Variables
    var selectedImageSection: Int?
    var selectedImageRow: Int?
    var gifImageView = UIImageView()
    
    //MARK: - Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.setupUI()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeViewSelector(_:))))
    }
    
    //MARK: - UI Methods
    private func setupUI()
    {
        self.view.backgroundColor            = .white
        let maxWidth                         = min(self.view.frame.size.width - (2 * Macros.InsetFromSuperView) , CGFloat(DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].imageWidth!))
        let maxHeight                        = min(self.view.frame.size.height - (2 * Macros.InsetFromSuperView) , CGFloat(DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].imageHeight!))
        self.gifImageView.frame.size         = CGSize(width: maxWidth , height: maxHeight)
        self.gifImageView.center             = self.view.center
        self.gifImageView.image              = DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].stillImage!
        self.gifImageView.layer.cornerRadius = Macros.GIFCornerRadius
        self.gifImageView.clipsToBounds      = true
        self.view.addSubview(self.gifImageView)
        self.updateGifImageInContainer(gifImageView: self.gifImageView)
    }
    
    private func updateGifImageInContainer(gifImageView: UIImageView)
    {
        if DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].gifImage == nil
        {
            let network = Network()
            network.downloadImage(fromURL: DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].imageGIFUrlString!)
            { (data) in
                if let imageData = data
                {
                    let downloadedImage = UIImage.gifImageWithData(imageData)
                    DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].gifImage = downloadedImage
                    DispatchQueue.main.async
                    {
                        gifImageView.image = downloadedImage
                    }
                }
            }
        }
        else
        {
            gifImageView.image = DataService.instance.allImagesModelSectionArray[self.selectedImageSection!][self.selectedImageRow!].gifImage
        }
    }
    
    //MARK: - Selectors
    @objc func closeViewSelector(_ tap: UITapGestureRecognizer)
    {
        self.gifImageView.frame = self.view.frame
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
}
