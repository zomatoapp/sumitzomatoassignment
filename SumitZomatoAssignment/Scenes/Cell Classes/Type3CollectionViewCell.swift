//
//  Type3CollectionViewCell.swift
//  SumitZomatoAssignment
//
//  Created by Sumit Makkar on 29/12/18.
//  Copyright © 2018 Sumit Makkar. All rights reserved.
//

import UIKit

class Type3CollectionViewCell: UICollectionViewCell
{
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.backgroundColor = .gray
        self.layer.cornerRadius = Macros.CornerRadius
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    func loadWithImage(named imageData: UIImage)
    {
        let backgroundImageView        = UIImageView.init(image: imageData)
        backgroundImageView.frame      = .zero
        backgroundImageView.frame.size = self.frame.size
        self.clipsToBounds             = true
        self.addSubview(backgroundImageView)
    }
}
